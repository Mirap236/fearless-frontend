import React, { useEffect, useState } from 'react'

function ConferenceForm() {
  const [locations, setLocations] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }
  const [name, setName] = useState('');
  function handleNameChange(event) {
    const value = event.target.value;
    setName(value);
  }
  const [starts, setStarts] = useState('');
  function handleStartsChange(event) {
    const value = event.target.value;
    setStarts(value);
  }
  const [ends, setEnds] = useState('');
  function handleEndsChange(event) {
    const value = event.target.value;
    setEnds(value);
  }
  const [description, setDescription] = useState('');
  function handleDescriptionChange(event) {
    const value = event.target.value;
    setDescription(value);
  }
  const [max_presentations, setMaxPresentations] = useState('');
  function handleMaxPresentationsChange(event) {
    const value = event.target.value;
    setMaxPresentations(value);
  }
  const [max_attendees, setMaxAttendees] = useState('');
  function handleMaxAttendeesChange(event) {
    const value = event.target.value;
    setMaxAttendees(value);
  }
  const [location, setLocation] = useState('');
  function handleLocationChange(event) {
    const value = event.target.value;
    setLocation(value);
  }
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = parseInt(max_presentations);
    data.max_attendees = parseInt(max_attendees);
    data.location = location;

    const conferenceUrl = 'http://localhost:8000/api/conferences/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        const newConference = await response.json;

        setName('');
        setStarts('');
        setEnds('');
        setDescription('');
        setMaxPresentations('');
        setMaxAttendees('');
        setLocation('');
      } else {
        console.error(`Error: ${response.status} ${response.statusText}`);
      }
    } catch (error) {
      console.error('Fetch error:', error);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartsChange} value={starts} placeholder="Starts" required type="date" name="starts" id="conference-start" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEndsChange} value={ends} placeholder="Ends" required type="date" name="ends" id="conference-end" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea onChange={handleDescriptionChange} value={description} placeholder="" required type="textarea" name="description" id="desc" className="form-control" rows="3"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxPresentationsChange} value={max_presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max-presentations" className="form-control" />
              <label htmlFor="max_presentations">Max presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxAttendeesChange} value={max_attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max-attendees" className="form-control" />
              <label htmlFor="max_attendees">Max attendees</label>
            </div>
            <div className="mb-3">
              <select required onChange={handleLocationChange} value={location} id="location" name="location" className="form-select">
                <option>Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ConferenceForm;
