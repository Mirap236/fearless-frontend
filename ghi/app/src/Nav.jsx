import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <>
      <nav className="navbar fixed-top navbar-expand-lg bg-body-tertiary border-bottom border-body shadow p-3 mb-5 bg-body-tertiary rounded" data-bs-theme="dark">
        <div className="container-fluid">
          <a className="navbar-brand link-dark" href="/">Conference GO!</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <div className="navbar-nav">
              <NavLink className="nav-link link-dark active" aria-current="page" to="/">Home</NavLink>
              <NavLink className="nav-link link-dark" aria-current="page" to="/locations/new">New location</NavLink>
              <NavLink className="nav-link link-dark" aria-current="page" to="/conferences/new">New Conference</NavLink>
              <NavLink className="nav-link link-dark" aria-current="page" to="/presentations/new">New Presentation</NavLink>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
}
export default Nav;
