function createCard(name, description, pictureUrl, startDate, endDate, location) {
  return `
        <div class="col-md-6 col-lg-4 masonry-column mb-5">
            <div class="card shadow">
                <div class="image-container" style="height: 300px; background-size: contain; overflow: hidden;">
                  <img src="${pictureUrl}" class="card-img-top" style="height: 300px; object-fit: cover;">
                </div>
                <div class="card-body" style="min-height: 240px;">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-4 text-muted">${location}</h6>
                <p class="card-text" style="overflow: hidden; display: -webkit-box; -webkit-line-clamp: 3; -webkit-box-orient: vertical;">${description}</p>
                </div>
                <div class="card-footer text-muted">
                    ${startDate} - ${endDate}
                </div>
            </div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error('Response not ok');
    } else {
      const data = await response.json();
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const location = details.conference.location.name;
          const startDate = new Date(details.conference.starts);
          const startMonth = startDate.getUTCMonth() + 1;
          const startDay = startDate.getUTCDate();
          const startYear = startDate.getUTCFullYear();
          const theStartDate = `${startMonth}/${startDay}/${startYear}`;
          const endDate = new Date(details.conference.ends);
          const endMonth = endDate.getUTCMonth() + 1;
          const endDay = endDate.getUTCDate();
          const endYear = endDate.getUTCFullYear();
          const theEndDate = `${endMonth}/${endDay}/${endYear}`;
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const html = createCard(name, description, pictureUrl, theStartDate, theEndDate, location);
          const column = document.querySelector('.row');
          column.innerHTML += html;
        }
      }
    }
  } catch (e) {
    console.error(e);
  }
});
